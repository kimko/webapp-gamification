angular.module('app', ['ui.bootstrap']);
angular.module('app').controller("MainController", function($scope, $timeout, $interval){
    var vm = this;

// DATA
    vm.e1rgb = [
    {
        number: 1,
        vid: 'vid/rgb/p1e1.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/rgb/p2e1.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/rgb/p3e1.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/rgb/p4e1.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/rgb/p5e1.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/rgb/p6e1.mp4',
        rank: 0,
    }
    ];
    vm.e1skel = [
    {
        number: 1,
        vid: 'vid/skel/p1e1.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/skel/p2e1.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/skel/p3e1.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/skel/p4e1.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/skel/p5e1.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/skel/p6e1.mp4',
        rank: 0,
    }
    ];
    vm.e1depth = [
    {
        number: 1,
        vid: 'vid/depth/p1e1.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/depth/p2e1.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/depth/p3e1.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/depth/p4e1.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/depth/p5e1.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/depth/p6e1.mp4',
        rank: 0,
    }
    ];
    vm.e1vr = [
    {
        number: 1,
        vid: 'vid/vr/p1e1.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/vr/p2e1.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/vr/p3e1.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/vr/p4e1.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/vr/p5e1.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/vr/p6e1.mp4',
        rank: 0,
    }
    ];

    vm.e2rgb = [
    {
        number: 1,
        vid: 'vid/rgb/p1e2.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/rgb/p2e2.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/rgb/p3e2.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/rgb/p4e2.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/rgb/p5e2.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/rgb/p6e2.mp4',
        rank: 0,
    }
    ];
    vm.e2skel = [
    {
        number: 1,
        vid: 'vid/skel/p1e2.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/skel/p2e2.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/skel/p3e2.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/skel/p4e2.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/skel/p5e2.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/skel/p6e2.mp4',
        rank: 0,
    }
    ];
    vm.e2depth = [
    {
        number: 1,
        vid: 'vid/depth/p1e2.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/depth/p2e2.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/depth/p3e2.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/depth/p4e2.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/depth/p5e2.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/depth/p6e2.mp4',
        rank: 0,
    }
    ];
    vm.e2vr = [
    {
        number: 1,
        vid: 'vid/vr/p1e2.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/vr/p2e2.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/vr/p3e2.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/vr/p4e2.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/vr/p5e2.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/vr/p6e2.mp4',
        rank: 0,
    }
    ];

    vm.e3rgb = [
    {
        number: 1,
        vid: 'vid/rgb/p1e3.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/rgb/p2e3.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/rgb/p3e3.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/rgb/p4e3.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/rgb/p5e3.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/rgb/p6e3.mp4',
        rank: 0,
    }
    ];
    vm.e3skel = [
    {
        number: 1,
        vid: 'vid/skel/p1e3.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/skel/p2e3.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/skel/p3e3.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/skel/p4e3.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/skel/p5e3.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/skel/p6e3.mp4',
        rank: 0,
    }
    ];
    vm.e3depth = [
    {
        number: 1,
        vid: 'vid/depth/p1e3.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/depth/p2e3.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/depth/p3e3.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/depth/p4e3.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/depth/p5e3.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/depth/p6e3.mp4',
        rank: 0,
    }
    ];
    vm.e3vr = [
    {
        number: 1,
        vid: 'vid/vr/p1e3.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/vr/p2e3.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/vr/p3e3.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/vr/p4e3.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/vr/p5e3.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/vr/p6e3.mp4',
        rank: 0,
    }
    ];

    vm.e4rgb = [
    {
        number: 1,
        vid: 'vid/rgb/p1e4.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/rgb/p2e4.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/rgb/p3e4.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/rgb/p4e4.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/rgb/p5e4.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/rgb/p6e4.mp4',
        rank: 0,
    }
    ];
    vm.e4skel = [
    {
        number: 1,
        vid: 'vid/skel/p1e4.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/skel/p2e4.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/skel/p3e4.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/skel/p4e4.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/skel/p5e4.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/skel/p6e4.mp4',
        rank: 0,
    }
    ];
    vm.e4depth = [
    {
        number: 1,
        vid: 'vid/depth/p1e4.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/depth/p2e4.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/depth/p3e4.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/depth/p4e4.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/depth/p5e4.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/depth/p6e4.mp4',
        rank: 0,
    }
    ];
    vm.e4vr = [
    {
        number: 1,
        vid: 'vid/vr/p1e4.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/vr/p2e4.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/vr/p3e4.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/vr/p4e4.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/vr/p5e4.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/vr/p6e4.mp4',
        rank: 0,
    }
    ];

    vm.e5rgb = [
    {
        number: 1,
        vid: 'vid/rgb/p1e5.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/rgb/p2e5.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/rgb/p3e5.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/rgb/p4e5.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/rgb/p5e5.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/rgb/p6e5.mp4',
        rank: 0,
    }
    ];
    vm.e5skel = [
    {
        number: 1,
        vid: 'vid/skel/p1e5.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/skel/p2e5.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/skel/p3e5.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/skel/p4e5.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/skel/p5e5.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/skel/p6e5.mp4',
        rank: 0,
    }
    ];
    vm.e5depth = [
    {
        number: 1,
        vid: 'vid/depth/p1e5.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/depth/p2e5.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/depth/p3e5.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/depth/p4e5.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/depth/p5e5.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/depth/p6e5.mp4',
        rank: 0,
    }
    ];
    vm.e5vr = [
    {
        number: 1,
        vid: 'vid/vr/p1e5.mp4',
        rank: 0,
    },
    {
        number: 2,
        vid: 'vid/vr/p2e5.mp4',
        rank: 0,
    },
    {
        number: 3,
        vid: 'vid/vr/p3e5.mp4',
        rank: 0,
    },
    {
        number: 4,
        vid: 'vid/vr/p4e5.mp4',
        rank: 0,
    },
    {
        number: 5,
        vid: 'vid/vr/p5e5.mp4',
        rank: 0,
    },
    {
        number: 6,
        vid: 'vid/vr/p6e5.mp4',
        rank: 0,
    }
    ];

// REFERENZDATA
    vm.Re1rgb = [
        {
            number: 1,
            rank: 1,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 4,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 5,
        }
        ];
    vm.Re1skel = [
        {
            number: 1,
            rank: 1,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 5,
        },
        {
            number: 4,
            rank: 4,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 6,
        }
        ];
    vm.Re1depth = [
        {
            number: 1,
            rank: 1,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 4,
        },
        {
            number: 5,
            rank: 5,
        },
        {
            number: 6,
            rank: 2,
        }
        ];
    vm.Re1vr = [
        {
            number: 1,
            rank: 4,
        },
        {
            number: 2,
            rank: 1,
        },
        {
            number: 3,
            rank: 3,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 6,
        }
        ];

    vm.Re2rgb = [
        {
            number: 1,
            rank: 4,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 5,
        },
        {
            number: 4,
            rank: 6,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 1,
        }
        ];
    vm.Re2skel = [
        {
            number: 1,
            rank: 3,
        },
        {
            number: 2,
            rank: 4,
        },
        {
            number: 3,
            rank: 1,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 6,
        }
        ];
    vm.Re2depth = [
        {
            number: 1,
            rank: 6,
        },
        {
            number: 2,
            rank: 1,
        },
        {
            number: 3,
            rank: 4,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 3,
        }
        ];
    vm.Re2vr = [
        {
            number: 1,
            rank: 3,
        },
        {
            number: 2,
            rank: 2,
        },
        {
            number: 3,
            rank: 4,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 1,
        },
        {
            number: 6,
            rank: 6,
        }
        ];

    vm.Re3rgb = [
        {
            number: 1,
            rank: 4,
        },
        {
            number: 2,
            rank: 1,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 3,
        }
        ];
    vm.Re3skel = [
        {
            number: 1,
            rank: 2,
        },
        {
            number: 2,
            rank: 1,
        },
        {
            number: 3,
            rank: 3,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 4,
        },
        {
            number: 6,
            rank: 6,
        }
        ];
    vm.Re3depth = [
        {
            number: 1,
            rank: 3,
        },
        {
            number: 2,
            rank: 2,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 4,
        },
        {
            number: 6,
            rank: 1,
        }
        ];
    vm.Re3vr = [
        {
            number: 1,
            rank: 2,
        },
        {
            number: 2,
            rank: 1,
        },
        {
            number: 3,
            rank: 5,
        },
        {
            number: 4,
            rank: 4,
        },
        {
            number: 5,
            rank: 3,
        },
        {
            number: 6,
            rank: 6,
        }
        ];

    vm.Re4rgb = [
        {
            number: 1,
            rank: 1,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 4,
        }
        ];
    vm.Re4skel = [
        {
            number: 1,
            rank: 1,
        },
        {
            number: 2,
            rank: 4,
        },
        {
            number: 3,
            rank: 3,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 2,
        },
        {
            number: 6,
            rank: 6,
        }
        ];
    vm.Re4depth = [
        {
            number: 1,
            rank: 5,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 6,
        },
        {
            number: 4,
            rank: 2,
        },
        {
            number: 5,
            rank: 1,
        },
        {
            number: 6,
            rank: 4,
        }
        ];
    vm.Re4vr = [
        {
            number: 1,
            rank: 4,
        },
        {
            number: 2,
            rank: 5,
        },
        {
            number: 3,
            rank: 2,
        },
        {
            number: 4,
            rank: 3,
        },
        {
            number: 5,
            rank: 1,
        },
        {
            number: 6,
            rank: 6,
        }
        ];

    vm.Re5rgb = [
        {
            number: 1,
            rank: 5,
        },
        {
            number: 2,
            rank: 2,
        },
        {
            number: 3,
            rank: 4,
        },
        {
            number: 4,
            rank: 6,
        },
        {
            number: 5,
            rank: 3,
        },
        {
            number: 6,
            rank: 1,
        }
        ];
    vm.Re5skel = [
        {
            number: 1,
            rank: 5,
        },
        {
            number: 2,
            rank: 2,
        },
        {
            number: 3,
            rank: 3,
        },
        {
            number: 4,
            rank: 4,
        },
        {
            number: 5,
            rank: 1,
        },
        {
            number: 6,
            rank: 6,
        }
        ];
    vm.Re5depth = [
        {
            number: 1,
            rank: 6,
        },
        {
            number: 2,
            rank: 3,
        },
        {
            number: 3,
            rank: 2,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 4,
        },
        {
            number: 6,
            rank: 1,
        }
        ];
    vm.Re5vr = [
        {
            number: 1,
            rank: 4,
        },
        {
            number: 2,
            rank: 2,
        },
        {
            number: 3,
            rank: 3,
        },
        {
            number: 4,
            rank: 5,
        },
        {
            number: 5,
            rank: 1,
        },
        {
            number: 6,
            rank: 6,
        }
        ];

    vm.Rexercise1 = [vm.Re1rgb, vm.Re1skel, vm.Re1depth, vm.Re1vr];
    vm.Rexercise2 = [vm.Re2rgb, vm.Re2skel, vm.Re2depth, vm.Re2vr];
    vm.Rexercise3 = [vm.Re3rgb, vm.Re3skel, vm.Re3depth, vm.Re3vr];
    vm.Rexercise4 = [vm.Re4rgb, vm.Re4skel, vm.Re4depth, vm.Re4vr];
    vm.Rexercise5 = [vm.Re5rgb, vm.Re5skel, vm.Re5depth, vm.Re5vr];

    vm.Rexercises = [vm.Rexercise1, vm.Rexercise2, vm.Rexercise3, vm.Rexercise4, vm.Rexercise5];

// EXERCISE ARRAYS
    vm.exercise1 = [vm.e1rgb, vm.e1skel, vm.e1depth, vm.e1vr];
    vm.exercise2 = [vm.e2rgb, vm.e2skel, vm.e2depth, vm.e2vr];
    vm.exercise3 = [vm.e3rgb, vm.e3skel, vm.e3depth, vm.e3vr];
    vm.exercise4 = [vm.e4rgb, vm.e4skel, vm.e4depth, vm.e4vr];
    vm.exercise5 = [vm.e5rgb, vm.e5skel, vm.e5depth, vm.e5vr];

    vm.exercises = [
        {array: vm.exercise1, pic: 'img/ex1.png', name: 'Squats'},
        {array: vm.exercise3, pic: 'img/ex3.png', name: 'Band Stretch'}, //Standing IT Band Stretch
        {array: vm.exercise2, pic: 'img/ex2.png', name: 'Lateral Lunges'},
        {array: vm.exercise4, pic: 'img/ex4.png', name: 'Forward Lunges'},
        {array: vm.exercise5, pic: 'img/ex5.png', name: 'Reverse Lunges'}
    ];

// Auswahlmöglichkeiten zur Bestimmung von 4 Teilnehmers aus 6 möglichen Teilnehmern
    vm.set = [
        [1,2,3,4],
        [1,3,2,4],
        [1,4,3,2],

        [1,2,4,5],
        [1,4,2,5],
        [1,5,4,2],

        [1,2,3,6],
        [1,3,2,6],
        [1,6,3,2],

        [1,2,4,5],
        [1,4,2,5],
        [1,5,4,2],

        [1,2,4,6],
        [1,4,2,6],
        [1,6,4,2],

        [1,2,5,6],
        [1,5,2,6],
        [1,6,5,2],

        [1,3,4,5],
        [1,4,3,5],
        [1,5,4,3],

        [1,3,4,6],
        [1,4,3,6],
        [1,6,4,3],

        [1,3,5,6],
        [1,5,3,6],
        [1,6,5,3],

        [1,4,5,6],
        [1,5,4,6],
        [1,6,5,4],

        [2,3,4,5],
        [2,4,3,5],
        [2,5,4,3],

        [2,3,4,6],
        [2,4,3,6],
        [2,6,4,3],

        [2,3,5,6],
        [2,5,3,6],
        [2,6,5,3],

        [2,4,5,6],
        [2,5,4,6],
        [2,6,5,4],

        [3,4,5,6],
        [3,5,4,6],
        [3,6,5,4]
    ];

// VARIABLES
    vm.buttontext = 'START';

    // Booleans
    vm.isDisabled = false;
    vm.voteDisabled = false;
    vm.showInfo = false;
    vm.showLevelUp = false;
    vm.countdownOn = true;
    vm.started = false;
    vm.finished = false;
    vm.formDisabled = false;
    vm.showFormAlert = false;
    vm.showForm = true;

    // Counter
    vm.eCounter = 0;
    vm.tCounter = 0;
    vm.rCounter = 0;
    vm.feedBackCounter = 0;
    vm.levelCapCounter = 1;
    vm.countdown = 20;

    // Stadium des Spiels: 0 = noch nicht angefangen, 1 = erklärung, 2 = turnier, 3 = fertig
    vm.state = 0;

    // Texte
    vm.ergebnis = "";
    vm.formular = "";
    vm.roundNames = ['First Preliminaries', 'Second Preliminaries', 'Third Place Playoff', 'Finals'];
    vm.levelTitle = [
        'Fledgling Judge', 
        'Up-and-coming Judge', 
        'Judging Expert', 
        'Judge of Justice', 
        'The Judge Legend', 
        'Judge Extraordinaire', 
        'God Of Judging'
    ];
    vm.feedBackAlerts = [
        'You finished your first tournament! This will be the start of something grand.',
        'Another one in the bag! You rule (pun intended).',
        'Almost finished with the first event. You can do this!',
        'You finished your first event! Great job. We cannot wait to see more of your judging!',
        'First tournament of this event is done! Beginnings are always special.',
        'You are on fire today! Keep up the good work.',
        'Not to play favourites, but you are really showing those other judges how it\'s done!',
        'Another event finished with excellent judging!',
        'This one was a bit tougher, but that did not bother you at all, apparently! Great job.',
        'Your judging is always a sight to see.',
        'Another tournament dutifully judged. Good work!',
        'This wraps up the third event! You\'re more than halfway done, not bad.',
        'And another "first tournament" is finished. Neat!',
        'Your effort really shows in the amazing amount of points you\'ve already collected!',
        'By now, this must be a walk in the park for you!',
        'You finished the fourth event! Only one event left, time to give it your all!',
        'Finished another first tournament! Do\'nt give up now!',
        'Great work as always! That trophy is as good as yours.',
        'Almost there, you can do it!',
        'YOU DID IT! Now let\'s go check out the award ceremony...'
    ];

    // Tracker
    vm.currentExercise = null;
    vm.currentTournament = null;

    // Progress, Points & Level
    vm.progress = 0;
    vm.points = 0;
    vm.pointsTotal = 0;
    vm.level = 1;
    vm.levelCaps = [25, 35, 50, 80, 100, 140, 200, 300];
    vm.levelCap = vm.levelCaps[0];

    // Voting Optionen
    vm.participants = null;
    vm.optiona = null;
    vm.optionb = null;

    //zur Bestimmung der Rangfolge
    vm.quali1 = null;
    vm.quali2 = null;
    vm.drop1 = null;
    vm.drop2 = null;
    vm.first = null;
    vm.second = null;
    vm.third = null;
    vm.last = null;

// FUNCTIONS

vm.shortcut = function(){
        vm.finished = true;
    }

// PROGRESSBAR
    vm.increaseProgress = function(amount){
        if((vm.progress+amount) <= 100){
            vm.progress+=amount;
        }
        else {
            vm.progress=100;
        } 
    }

// USER ID
    vm.generateUserID = function(){
        vm.userID = "";
        vm.possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(i = 0; i < 5; i++){
            vm.userID += vm.possibleCharacters.charAt(Math.floor(Math.random() * vm.possibleCharacters.length));
        }
    }

// STARTEN & WEITERMACHEN
    // Bestimme zufällige vier Teilnehmer
    vm.getParticipants = function(ar){
        // Wähle ein zufälliges Set aus dem set-Array
        vm.newArray = [];
        vm.rand = Math.floor(Math.random() * 44); 
        vm.chosenSet = vm.set[vm.rand];
        // erzeuge einen neuen Array mit den ausgewählten Elementen
        vm.newArray.push(ar[vm.chosenSet[0]-1], ar[vm.chosenSet[1]-1], 
           ar[vm.chosenSet[2]-1], ar[vm.chosenSet[3]-1]);
        return vm.newArray;
    }

    // wird nur einmal aufgerufen
    vm.spielInitialisieren = function(){
        if(vm.started == false){
            vm.setCurrentExercise(vm.exercise1, 1);
            vm.generateUserID();
            vm.started = true;
        }
    }

    // Spiel starten
    vm.start = function(){
        // INITALISIEREN
        if(vm.state == 0){
            vm.resetCountdown();
            vm.participants = vm.getParticipants(vm.currentTournament);
            vm.optiona = vm.participants[0];
            vm.optionb = vm.participants[1];
            vm.rCounter = 1;
            vm.tCounter = 1;
            vm.state = 1;
            vm.buttontext = 'NEXT TOURNAMENT';
            vm.isDisabled = true;
            vm.penaltyTimer = $timeout(vm.penalty, 15000); 
            vm.countdownOn = true;
        }
        // WEITER
        else if (vm.state == 1){
            vm.resetCountdown();
            vm.nextTournament();
            vm.participants = vm.getParticipants(vm.currentTournament);
            vm.optiona = vm.participants[0];
            vm.optionb = vm.participants[1];
            vm.rCounter = 1;
            if(vm.state == 1) { vm.isDisabled = true; }
            vm.voteDisabled = false;
            vm.showInfo = false;
            vm.countdownOn = true;
        }
    }

    vm.setCurrentExercise = function(ar, number){
        vm.eCounter = number;
        vm.currentExercise = ar;
        vm.tCounter = 1;
        vm.setCurrentTournament(ar[0]);
    }

    vm.setCurrentTournament = function(ar){
        vm.currentTournament = ar;
    }

    vm.nextTournament = function(){
        // wenn mitten in einem Event
        if(vm.tCounter < 4){
            vm.tCounter++;
            vm.setCurrentTournament(vm.currentExercise[vm.tCounter-1]);
        }
        // wenn am Ende eines Events
        else if (vm.tCounter == 4){
            vm.tCounter = 0;
            vm.nextExercise();
            vm.state = 0;
            vm.isDisabled = false;
            vm.buttontext = 'START';
        }
    }

    vm.nextExercise = function(){
        // während des Spiels
        if (vm.eCounter < 5){
            vm.eCounter++;
            vm.setCurrentExercise(vm.exercises[vm.eCounter-1].array, vm.eCounter);
        }
        // wenn das Spiel fertig ist
        else {
            vm.finished = true;
            $timeout.cancel(vm.penaltyTimer);
        }
    }

    // Zeigt die übergebenen Teilnehmer im Votingbereich an
    vm.show = function(a, b){
        vm.optiona = a;
        vm.optionb = b;
    }

// VOTING & AUSWERTUNG
    vm.vote = function(winner, loser){
        // löscht vorigen Timer und setzt neuen auf
        if(vm.rCounter < 4) {
            vm.resetCountdown(); 
            $timeout.cancel(vm.penaltyTimer);
            vm.penaltyTimer = $timeout(vm.penalty, 20000); 
        }
        // Ergebnisse werden je nachdem welche Runde ist weiterverwertet
        // First Preliminaries
        if(vm.rCounter == 1){
            vm.quali1 = winner;
            vm.drop1 = loser;
            vm.show(vm.participants[2], vm.participants[3]);
            vm.rCounter++;
        }
        // Second Preliminaries
        else if(vm.rCounter == 2){
            vm.quali2 = winner;
            vm.drop2 = loser;
            vm.show(vm.drop1, vm.drop2);
            vm.rCounter++;
        }
        // Third Place Playoff
        else if(vm.rCounter == 3){
            vm.third = winner;
            vm.last = loser;
            vm.show(vm.quali1, vm.quali2);
            vm.rCounter++;
        }
        // Finals
        else if(vm.rCounter == 4){
            vm.first = winner;
            vm.second = loser;
            vm.rCounter++;
            vm.rank();
            vm.increaseProgress(5);
            vm.isDisabled = false;
            vm.voteDisabled = true;
            vm.showInfo = true;
            $timeout.cancel(vm.penaltyTimer);
            vm.countdownOn = false;
            vm.feedBackCounter++;
            if(vm.tCounter == 4) {
                vm.buttontext = 'NEXT EVENT';
                if(vm.eCounter == 5) {
                    vm.buttontext = 'ATTEND AWARD CEREMONY';
                }
            }
            vm.saveResult();
            vm.sendTheMail(vm.ergebnis, vm.userID); 
        }
        vm.determinePoints(winner, loser);
        vm.checkLevel();
        vm.saveResult();
    }

    // Aktualisiert den Rang nach einem Turnier
    vm.rank = function(){
        vm.first.rank = 1;
        vm.second.rank = 2;
        vm.third.rank = 3;
        vm.last.rank = 4;
    }

    // Ergebnis als String abspeichern
    vm.saveResult = function(){
        vm.ergebnis = "User: " + vm.userID + " ||| Exercise: " + vm.exercises[vm.eCounter-1].name + 
        " (" + vm.eCounter+ "), Tournament: " + (vm.tCounter) + ", Result (first to last): " + 
        vm.first.number + ", " + vm.second.number + ", " + vm.third.number + ", " + vm.last.number
        + " (from Set: " + vm.chosenSet + ") ||| Points: " + vm.pointsTotal + " & Level: " + vm.level;
    }

    // weist jedem Array seinen jeweiligen Referenzarray für die Punktevergabe zu
    vm.getReferenceArray = function(){
        if(vm.eCounter == 1){
            if(vm.tCounter == 1) { return vm.Rexercise1[0];}
            else if(vm.tCounter == 2) { return vm.Rexercise1[1];}
            else if(vm.tCounter == 3) { return vm.Rexercise1[2];}
            else if(vm.tCounter == 4) { return vm.Rexercise1[3];}
        }
        else if(vm.eCounter == 2){
            if(vm.tCounter == 1) { return vm.Rexercise2[0];}
            else if(vm.tCounter == 2) { return vm.Rexercise2[1];}
            else if(vm.tCounter == 3) { return vm.Rexercise2[2];}
            else if(vm.tCounter == 4) { return vm.Rexercise2[3];}
        }
        else if(vm.eCounter == 3){
            if(vm.tCounter == 1) { return vm.Rexercise3[0];}
            else if(vm.tCounter == 2) { return vm.Rexercise3[1];}
            else if(vm.tCounter == 3) { return vm.Rexercise3[2];}
            else if(vm.tCounter == 4) { return vm.Rexercise3[3];}
        }
        else if(vm.eCounter == 4){
            if(vm.tCounter == 1) { return vm.Rexercise4[0];}
            else if(vm.tCounter == 2) { return vm.Rexercise4[1];}
            else if(vm.tCounter == 3) { return vm.Rexercise4[2];}
            else if(vm.tCounter == 4) { return vm.Rexercise4[3];}
        }
        else if(vm.eCounter == 5){
            if(vm.tCounter == 1) { return vm.Rexercise5[0];}
            else if(vm.tCounter == 2) { return vm.Rexercise5[1];}
            else if(vm.tCounter == 3) { return vm.Rexercise5[2];}
            else if(vm.tCounter == 4) { return vm.Rexercise5[3];}
        }
    }

// POINTS & LEVEL
    vm.increasePoints = function(amount){
        vm.points += amount;
        vm.pointsTotal += amount;
        vm.checkLevel();
    }

    vm.hideLevelUp = function() {
        vm.showLevelUp = false;
    };

    // Punktebestimmung; jedes Voting bringt 5 Punkte, zusätzliche 15 wenn es mit den Referenzwerten übereinstimmt
    vm.determinePoints = function(winner, loser){
        vm.points+=5;
        vm.pointsTotal+=5;
        vm.referenceArray = vm.getReferenceArray();
        if(vm.referenceArray[winner.number-1].rank < vm.referenceArray[loser.number-1].rank){
            vm.points+=15;
            vm.pointsTotal+=15;
        }
    }

    // Überprüfung, ob Level-Up ansteht
    vm.checkLevel= function(){
        if(vm.points >= vm.levelCap){
            // wenn Level-Up genau erreicht wird werden die Punkte zurückgesetzt...
            if(vm.points == vm.levelCap){ 
                vm.points = 0; 
            }
            // ansonsten werden die Überschüssigen Punkte wieder gutgeschrieben
            else if (vm.points > vm.levelCap){ 
                vm.points = vm.points - vm.levelCap; 
            }
            // Level-Up wird durchgeüfhrt
            vm.level++;
            vm.showLevelUp = true;
            // LevelCap wird hochgesetzt
            vm.levelCap += vm.levelCaps[vm.levelCapCounter];
            if (vm.levelCapCounter < 8) { 
                vm.levelCapCounter++; 
            }
        }
    }

// COUNTDOWN
    // zählt Countdown runter
    vm.onTimeout = function(){
        if(vm.countdown > 0) {vm.countdown--;}
        vm.mytimeout = $timeout(vm.onTimeout,1000);
    }
    vm.mytimeout = $timeout(vm.onTimeout,1000);
    
    // Countdown wird auf Anfangswert gesetzt
    vm.resetCountdown = function(){
        vm.countdown = 20;
    }

    // Puntkabzug, findet statt wenn Countdown nicht abgebrochen wird
    vm.penalty = function(){
        if (vm.points > 0) { vm.points -= 4; }
        if (vm.pointsTotal > 0) {  vm.pointsTotal -= 4; }
    }

// FORMULAR
    vm.submit= function(pGender, pAge, pOccupation, pComment){
        vm.formular = "User: " + vm.userID + ", Gender: " + pGender + ", Age: " + pAge + 
        ", Occupation: " + pOccupation + ", Comment: " + pComment;
        vm.sendTheForm(vm.formular, vm.userID);
        vm.formDisabled = true;
        vm.showFormAlert = true;
    }

    vm.hideForm = function(){
        vm.showForm = false;
    }

    vm.hideFormAlert = function(){
        vm.showFormAlert = false;
    }

// MAIL SENDEN
    // create new mandrill class with API key
    vm.m = new mandrill.Mandrill('ARfAk3J6ZOw5cyqBcKGhHg');

    // sende Ergebnis-Mail
    vm.sendTheMail = function(content, user) {
        vm.m.messages.send({
            "message": {
                "global_merge_vars": [
                {
                    "name": "ergebnis",
                    "content": content
                },
                {
                    "name": "userid",
                    "content": user
                }
                ],
                "from_email":"kimkbachelorthesis@gmail.com",
                "to":[{"email":"kimkbachelorthesis@gmail.com"}],
                "subject": "Ergebnisse *|USERID|*",
                "text": "*|ERGEBNIS|*"
            }
        }, function(res) {}, function(err) {});
    }

    // sende Formular-Mail
    vm.sendTheForm = function(formular, user) {
        vm.m.messages.send({
            "message": {
                "global_merge_vars": [
                {
                    "name": "form",
                    "content": formular
                },
                {
                    "name": "userid",
                    "content": user
                }
                ],
                "from_email":"kimkbachelorthesis@gmail.com",
                "to":[{"email":"kimkbachelorthesis@gmail.com"}],
                "subject": "Formular *|USERID|*",
                "text": "*|FORM|*"
            }
        }, function(res) {}, function(err) {});
    }
});

